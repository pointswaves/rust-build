use std::sync::atomic::{AtomicBool, Ordering};
use tokio::sync::Notify;

#[derive(Default)]
pub struct Event {
    notify: Notify,
    inner: AtomicBool,
}

impl Event {
    pub fn get(&self) -> bool {
        self.inner.load(Ordering::Acquire)
    }

    pub async fn wait(&self) {
        loop {
            let future = self.notify.notified();
            if self.get() {
                return;
            }
            future.await;
            if self.get() {
                return;
            }
        }
    }

    pub fn set(&self) {
        self.inner.store(true, Ordering::Relaxed);
        self.notify.notify_waiters();
    }
}
